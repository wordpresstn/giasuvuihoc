<?php
@session_start();
$_SESSION["captcha"] =  substr(sha1(mt_rand()), 17, 6);
$image = imagecreatetruecolor(150, 35);
$width = imagesx($image);
$height = imagesy($image);

$black = imagecolorallocate($image, 0, 0, 0);
$white = imagecolorallocate($image, 255, 255, 255);
$red = imagecolorallocatealpha($image, 255, 0, 0, 75);
$green = imagecolorallocatealpha($image, 0, 255, 0, 75);
$blue = imagecolorallocatealpha($image, 0, 0, 255, 75);
$violet = imagecolorallocatealpha($image, 100, 8, 181, 75);
$color1 = imagecolorallocatealpha($image, 16, 121, 62, 75);
$color2 = imagecolorallocatealpha($image,48, 11, 245, 75);

$values = array(
    ceil(rand(5, 145)),  ceil(rand(0, 35)),  // Point 1 (x, y)
    ceil(rand(5, 145)), ceil(rand(0, 35)), // Point 2 (x, y)
    ceil(rand(5, 145)),  ceil(rand(0, 35)),  // Point 3 (x, y)
    ceil(rand(5, 145)), ceil(rand(0, 35)),  // Point 4 (x, y)
    ceil(rand(5, 145)),  ceil(rand(0, 35)),  // Point 5 (x, y)
    ceil(rand(5, 145)),  ceil(rand(0, 35))  // Point 6 (x, y)
);
imagefilledrectangle($image, 0, 0, $width, $height, $white);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 50, 75, $color2);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 80, 85, $green);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 0, 15, $red);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 90, 5, $blue);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 50, 45, $color1);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 80, 25, $green);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 30, 55, $color2);
imagedashedline($image, ceil(rand(5, 145)),  ceil(rand(0, 35)), 60, 185, $blue);
imagefilledrectangle($image, 0, 0, $width, 0, $black);
imagefilledrectangle($image, $width - 1, 0, $width - 1, $height - 1, $black);
imagefilledrectangle($image, 0, 0, 0, $height - 1, $black);
imagefilledrectangle($image, 0, $height - 1, $width, $height - 1, $black);
imagefilledpolygon($image, $values, 6, $violet);
imagestring($image, 10, intval(($width - (strlen($_SESSION['captcha']) * 9)) / 2), intval(($height - 15) / 2), $_SESSION['captcha'], $black);

header('Content-type: image/jpeg');

imagejpeg($image);

imagedestroy($image);
?>

