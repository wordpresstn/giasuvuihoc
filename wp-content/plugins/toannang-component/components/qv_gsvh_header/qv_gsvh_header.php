<?php

if (!class_exists('TNCP_qv_gsvh_header')) {
    class TNCP_qv_gsvh_header extends TNCP_ToanNang
    {
        protected $options = [
                'gs_header_info' => ''
        ];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()  {
            $info = $this->getOption('gs_header_info');
            ?>

            <header class="qv_gsvh_header">
                <div class="group">
                    <div class="logo">
                        <?php
                        if(strlen(az_box_logo_primary())>0){
                            echo  az_box_logo_primary();
                        }else{  ?>
                            <a href="/" title="Trang Chủ">
                                <img src="<?= $this->getPath() ?>images/logo.png">
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="top">
                       <div class="width">
                           <div class="container-fluid">
                               <div class="row">
                                   <div class="col-md-offset-4 col-md-2 col-sm-4">
                                        <a href="mailto:<?php echo $info['email']; ?>" class="mail"><i class="fas fa-envelope"></i><?php echo $info['email']; ?></a>
                                   </div> 
                                   <div class="col-md-3 col-sm-4">
                                      <div class="hotline">
                                        <span>Hotline:</span>
                                        <a href="tel:<?php echo $info['hotline_1']; ?>"><?php echo $info['hotline_1']; ?></a>
                                        <a href="tel:<?php echo $info['hotline_2']; ?>"><?php echo $info['hotline_2']; ?></a>
                                      </div>
                                   </div>
                                   <div class="col-md-3 col-sm-4">
                                      <a href="#" class="register_gs"><i class="fas fa-user"></i>Đăng Ký Gia Sư</a>
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                    <div class="bottom">
                        <div class="width">
                           <div class="container-fluid">
                               <div class="row">
                                   <div class="col-md-offset-4 col-md-8">
                                       <!--MENU DESKTOP-->
                                        <div class="gsvh_header_nav">
                                            <?php
                                            wp_nav_menu( array(
                                                    'theme_location' => 'main-menu',
                                                    'container' => 'false',
                                                    'menu_id' => '',
                                                    'menu_class' => '',
                                                )
                                            ); ?>
                                        </div>

                                        <!--MENU MOBILE-->
                                        <div id="page" class="gsvh_header_mobile">
                                            <div class="header">
                                                <a href="#menu"><span><i class="fas fa-bars"></i></span></a>
                                            </div>
                                            <nav id="menu">
                                                <?php
                                                wp_nav_menu( array(
                                                        'theme_location' => 'main-menu',
                                                        'container' => 'false',
                                                        'menu_id' => '',
                                                        'menu_class' => '',
                                                    )
                                                ); ?>
                                            </nav>
                                        </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </header>
			<?php
        }
    }
}
