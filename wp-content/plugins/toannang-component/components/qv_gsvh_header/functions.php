<?php
function register_my_menus()
{
    register_nav_menus(
        array(
            'main-menu' => __('Main Menu', 'tn_component'),
        )
    );
}

add_action('after_setup_theme', 'register_my_menus');
