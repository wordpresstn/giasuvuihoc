
$('.qv_gsvh_banner .banner_slider').slick({
  arrows: true,
  dots: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  speed: 700,
  autoplay: true,
  autoplaySpeed: 4000,
  prevArrow:'<div class="btn-slider-left"><i class="fas fa-chevron-left"></i></div>',
  nextArrow:'<div class="btn-slider-right"><i class="fas fa-chevron-right"></i></div>'
});