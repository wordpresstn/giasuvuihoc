<?php

if (!class_exists('TNCP_qv_gsvh_banner')) {
    class TNCP_qv_gsvh_banner extends TNCP_ToanNang
    {
        protected $options = [];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()
        {
            ?>
            <div class="qv_gsvh_banner">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="banner_slider">
                                    <a href="#"><img src="<?php echo $this->getPath()?>images/slider_03.jpg"></a>
                                    <a href="#"><img src="<?php echo $this->getPath()?>images/slider_03.jpg"></a>
                                    <a href="#"><img src="<?php echo $this->getPath()?>images/slider_03.jpg"></a>
                                    <a href="#"><img src="<?php echo $this->getPath()?>images/slider_03.jpg"></a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="lopster">
                                    <h3>gia sư vui hoc</h3>
                                    <div class="group">
                                        <div class="box">
                                            <span>gia sư đăng nhập</span>
                                            <form>
                                                <input type="text" class="form-control" placeholder="Nhập số điện thoại:" name="">
                                                <button><i class="far fa-user"></i>Đăng nhập</button>
                                            </form>
                                        </div>
                                        <div class="box">
                                            <span>nhận lớp online</span>
                                            <form>
                                                <input type="text" class="form-control" placeholder="Nhập mã lớp" name="">
                                                <input type="text" class="form-control" placeholder="Nhập mã bảo mật:" name="">
                                                <button><i class="fas fa-search"></i>kiểm tra</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
