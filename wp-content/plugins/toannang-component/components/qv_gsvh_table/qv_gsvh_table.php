<?php

if (!class_exists('TNCP_qv_gsvh_table')) {
    class TNCP_qv_gsvh_table extends TNCP_ToanNang
    {
        protected $options = [];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()
        {
            ?>

            <div class="qv_gsvh_table">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12">
                                <span class="title">
                                    thông tin hồ sơ
                                </span>
                                <div class="name">
                                    Hồ sơ của gia sư:<strong>Nguyễn trung tín</strong>
                                </div>
                                <strong class="ds">DANH SÁCH CÁC LỚP ĐÃ ĐĂNG KÝ</strong>
                                <form>
                                    <div class="list_table">
                                        <table>
                                            <tr>
                                                <th>Mã lớp</th>
                                                <th>Lớp dạy</th>
                                                <th>Môn học</th>
                                                <th>Địa chỉ</th>
                                                <th>Mức lương</th>
                                                <th>Số buổi</th>
                                                <th>Thời gian</th>
                                                <th>Yêu cầu</th>
                                                <th>Hình thức/Phí</th>
                                                <th>Trạng thái</th>
                                            </tr>
                                            <tr>
                                                <td>432/BTH</td>
                                                <td><span>11 + 12</span></td>
                                                <td>Toán</td>
                                                <td>Ung Văn Khiêm, Phường 25, Quận Bình Thạnh(Xem bản đồ)</td>
                                                <td><span>2,000,000₫</span> /2người/tháng</td>
                                                <td>2(1b=1,5h) Buổi/Tuần</td>
                                                <td>Sắp Xếp (sắp xếp)</td>
                                                <td>Giáo viên Nam, Nữ</td>
                                                <td>Chuyển khoản 35% = <span>700,000₫</span></td>
                                                <td><strong>Đã nhận lớp</strong></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <button>Đăng ký thêm lớp dạy mới >></button>
                                    </div>
                                </form>
                                <div class="text">
                                    <p><strong>Lưu ý:</strong> Trung tâm Gia Sư Toàn Tâm ưu tiên người đủ điều kiện, đóng lệ phí sớm. Trước khi tới trung tâm hoặc chuyển khoản hãy gọi số <span>0902929127 hoặc 0907971009</span> để kiểm tra trạng thái lớp chính xác.</p>
                                    <p>Khi đã Download Giấy giới thiệu, vui lòng photo giấy giới thiệu và bằng cấp hoặc thẻ <i>SV + CMND</i> ra thành 1 bản để vào phong bì. Phía người gửi ghi: Trung tâm gia sư Thăng Long, người nhận ghi :Tên + Địa chỉ + Số điện thoại Phụ Huynh/Học Viên. Bạn gọi điện liên hệ ngay với Phụ Huynh/Học Viên hẹn giờ gặp và gửi phong bì cho Phụ Huynh/Học Viên, Bạn cố gắng gặp Phụ Huynh/Học Viên trong thời gian sớm nhất để tránh trường hợp lớp bị hủy. Sau khi liên hệ và hẹn gặp Phụ Huynh/Học Viên Bạn phải báo về cho trung tâm biết. Nếu cố tình kéo dài thời gian gặp, thay đổi thời gian dạy, đòi ứng lương trước hoặc đòi tăng lương mà lớp bị hủy Bạn tự chịu trách nhiệm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
