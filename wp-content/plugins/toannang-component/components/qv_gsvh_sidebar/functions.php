<?php

    register_nav_menus(
        array(

            'phuhuynh-menu' => __('Dành cho phụ huynh', 'tn_component'),
            'giasunh-menu' => __('Dành cho gia sư', 'tn_component'),
        )
    );


add_action('after_setup_theme', 'register_my_menus');

function sidebar_giasuvuihoc() {

    ob_start(); ?>


        <div class="qv_content_list">
            <div class="list">
                <h3>Dành cho phụ huynh</h3>
                <?php
                wp_nav_menu( array(
                        'theme_location' => 'phuhuynh-menu',
                        'container' => 'false',
                        'menu_id' => '',
                        'menu_class' => '',
                    )
                ); ?>
            </div>
            <div class="list">
                <h3>Dành cho gia sư vui học</h3>
                <?php
                wp_nav_menu( array(
                        'theme_location' => 'giasunh-menu',
                        'container' => 'false',
                        'menu_id' => '',
                        'menu_class' => '',
                    )
                ); ?>
            </div>
            <div class="group">
                <?php $banner = get_field('gs_home_banner_doc','option') ?>
                <?php if($banner):
                    foreach ($banner as $item){
                        ?>
                        <div class="item">
                            <a href="<?php echo $item['link']?>">
                                <img src="<?php echo $item['img']['url'];?>" alt="">
                                <span><?php echo $item['title']?></span>
                            </a>
                        </div>
                    <?php } endif;?>
            </div>
        </div>


  <?php   $list_post = ob_get_contents();

    ob_end_clean();

    return $list_post;
}
add_shortcode('echo_sidebar_giasuvuihoc', 'sidebar_giasuvuihoc');