<?php

if (!function_exists('lept_search_tutor')):
    add_action('search_tutor', 'lept_search_tutor', 10, 1);
    function lept_search_tutor($data = array())
    {
        $tax_subjects = get_terms(array(
            'taxonomy' => 'tax_subjects',
            'hide_empty' => false,
        ));
        $tax_monhocs = get_terms(array(
            'taxonomy' => 'tax_monhoc',
            'hide_empty' => false,
        ));
        $tax_locations = get_terms(array(
            'taxonomy' => 'tax_location',
            'hide_empty' => false,
        ));
        $tax_times = get_terms(array(
            'taxonomy' => 'tax_time',
            'hide_empty' => false,
        ));
        $tax_truongs = get_terms(array(
            'taxonomy' => 'tax_truong',
            'hide_empty' => false,
        ));
        $tax_levels = get_terms(array(
            'taxonomy' => 'tax_level',
            'hide_empty' => false,
        ));
        $genders = ['Nam', 'Nữ'];

        $fitter = array();
        $tax_subject = isset($_GET['tax_subjects']) ? $_GET['tax_subjects'] : '';
        $tax_monhoc = isset($_GET['tax_monhoc']) ? $_GET['tax_monhoc'] : '';
        $tax_location = isset($_GET['tax_location']) ? $_GET['tax_location'] : '';
        $tax_level = isset($_GET['tax_level']) ? $_GET['tax_level'] : '';
        $tax_truong = isset($_GET['tax_truong']) ? $_GET['tax_truong'] : '';
        $gender = isset($_GET['gender']) ? $_GET['gender'] : '';
        $fitter = array(
            'tax_subjects' => $tax_subject,
            'tax_monhoc' => $tax_monhoc,
            'tax_level' => $tax_level,
            'tax_truong' => $tax_truong,
            'gender' => $gender,
        );
        $tutors = get_users(['role__in' => ['author', 'subscriber']]);
        ?>
        <div class="qv_archive_giasu">
            <form action="/gia-su/" method="get">
                <div class="list">
                    <select class="form-control" name="tax_subjects">
                        <option value="">Chọn lớp</option>
                        <?php foreach ($tax_subjects as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>" <?= ($value->term_id == $tax_subject) ? 'selected' : '' ?> ><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-control" name="tax_monhoc">
                        <option value="">Chọn môn</option>
                        <?php foreach ($tax_monhocs as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>" <?= ($value->term_id == $tax_monhoc) ? 'selected' : '' ?>><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-control" name="tax_location">
                        <option value="">Tất cả khu vực</option>
                        <?php foreach ($tax_locations as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>" <?= ($value->term_id == $tax_location) ? 'selected' : '' ?>><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-control" name="tax_level">
                        <option value="">Hiện là</option>
                        <?php foreach ($tax_levels as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>" <?= ($value->term_id == $tax_level) ? 'selected' : '' ?>><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-control" name="tax_truong">
                        <option value="">Là SV trường</option>
                        <?php foreach ($tax_truongs as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>" <?= ($value->term_id == $tax_truong) ? 'selected' : '' ?>><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-control" name="gender">
                        <option value="">Giới tính</option>
                        <?php foreach ($genders as $key => $value) { ?>
                            <option value="<?= $value ?>" <?= ($value == $gender) ? 'selected' : '' ?>><?= $value ?></option>
                        <?php } ?>
                    </select>
                    <button type="submit">Tìm gia sư</button>
                </div>
            </form>
            <div class="group">
                <?php foreach ($tutors as $tutor) { ?>
                    <?php
                    $meta = get_user_meta($tutor->ID);                 
                    ?>
                    <div class="item">
                        <div class="img">
                            <img src="<?= isset($meta['user_avatar'][0]) ? $meta['user_avatar'][0] : ''; ?>">
                        </div>
                        <div class="desc"><strong>Mã số : </strong><?= $tutor->ID ?></div>
                        <div class="desc"><strong>Tên gia sư
                                : </strong><a><?= isset($meta['display_name'][0]) ? $meta['display_name'][0] : ''; ?></a>
                        </div>
                        <div class="desc"><strong>Giới tính
                                : </strong><?= isset($meta['gender'][0]) ? $meta['gender'][0] : ''; ?></div>
                        <div class="desc"><strong>Năm sinh
                                : </strong><?= isset($meta['yearborn'][0]) ? $meta['yearborn'][0] : ''; ?></div>
                        <div class="desc"><strong>Trình độ: </strong>
                            <?php foreach ($tax_levels as $key => $value) { ?>
                                 <?= (isset($meta['tax_level'][0]) &&  $value->term_id == $meta['tax_level'][0]) ? $value->name: '' ?>
                            <?php } ?>
                        </div>
                        <div class="desc"><strong>Trường: </strong>
                            <?php foreach ($tax_truongs as $key => $value) { ?>
                                 <?= (isset($meta['tax_truong'][0]) && $value->term_id == $meta['tax_truong'][0]) ? $value->name : '' ?>
                            <?php } ?>
                        </div>
                        <div class="desc"><strong>Chuyên ngành
                                : </strong><?= isset($meta['specialsize'][0]) ? $meta['specialsize'][0] : ''; ?></div>
                        <div class="desc">
                            <strong>Nhận dạy : </strong>
                            <?php
                            $truongs = isset($meta['tax_truong'][0])?unserialize($meta['tax_truong'][0]):array();
                            foreach ($tax_truongs as $key => $value) {?>
                                <?php if ( count($truongs) > 0 &&  in_array($value->term_id,$truongs)) { ?>
                                    <span><?= $value->name ?></span>
                                <?php } ?>
                            <?php } ?>
                        </div>
                        <div class="desc">
                            <strong>Các môn : </strong>

                        </div>
                        <div class="desc">
                            <strong>Khu vực : </strong>

                        </div>
                        <div class="desc"><strong>Lương yêu cầu : </strong><?= isset($meta['salary'][0]) ? $meta['salary'][0] : ''; ?></div>
                        <div class="desc"><strong>Thông tin khác : </strong><?= isset($meta['comment'][0]) ? $meta['comment'][0] : ''; ?></div>
                        <div class="button_item">
                            <a href="#"><i class="fas fa-edit"></i> Xem</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>

        <?php
    }
endif;
?>