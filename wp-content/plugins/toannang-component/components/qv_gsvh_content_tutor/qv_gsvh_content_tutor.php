<?php

if (!class_exists('TNCP_qv_gsvh_content_tutor')) {
    class TNCP_qv_gsvh_content_tutor extends TNCP_ToanNang
    {
        protected $options = [];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()
        {
            ?>

            <div class="qv_gsvh_content_tutor">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                            <?php echo do_shortcode('[echo_sidebar_giasuvuihoc]'); ?>
                            </div>
                            <div class="col-md-9">
                                <!-- TRANG GIA SƯ VUI HỌC -->
                                <?php do_action('search_tutor', array()); ?>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
