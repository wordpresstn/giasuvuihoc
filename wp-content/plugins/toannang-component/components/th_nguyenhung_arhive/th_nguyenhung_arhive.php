<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */

if (!class_exists('TNCP_th_nguyenhung_arhive')){
    class TNCP_th_nguyenhung_arhive extends TNCP_ToanNang{

        protected $options = [

        ];
        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        /*Add html to Render*/
        public function render(){ ?>

            <?php global $post; ?>
            <div id="th_nguyenhung_arhive" class="th_nguyenhung_arhive">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="header-title-dv">
                                <span class="title-archive"><?php the_archive_title()?></span>
                            </div>
                            <div class="">
                                <?php while(have_posts()): the_post();?>
                                    <div class="item-post wow fadeInRight">
                                        <div class="item">
                                            <div class="post-thumbnail">
                                                <a href="<?php echo get_permalink()?>" title="<?php echo get_the_title() ?>">
                                                    <img src="
                                                <?php if ( has_post_thumbnail() ) { ?>
                                              <?php echo get_the_post_thumbnail_url(get_the_ID(),'size380x285')?>"/>
                                                    <?php }else { ?>
                                                        <img src="<?php echo $this->getPath(); ?>/images/default.jpg">
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="box-info">
                                                <h5 class="title"><a href="<?php echo get_permalink()?>"><?php the_title()?></a></h5>
                                                <p class="info-text fo-avo">
                                                    <span><i class="fas fa-calendar-alt"></i> <?php echo get_the_date('d/m/Y',get_the_ID())?>  </span>

                                                </p>
                                                <div class="post_excerpt">
                                                    <?php
                                                    echo wp_trim_words( get_the_content(), 40, '...' );
                                                    ?>
                                                </div>
                                                <div class="view-detail">
                                                    <a href="<?php echo get_permalink()?>">Xem thêm <i class="fas fa-chevron-right"></i> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                            <div class="pagination">
                                <?php wp_pagenavi();?>
                            </div>
                        </div>
                        <div class="col-lg-3 widget-sidebars" id="single-sidebar">
                            <aside id="text-2" class="widget widget_text wow fadeInLeft" style="visibility: visible; animation-name: fadeInLeft;">
                                <h3 class="widget-title text-center">Tin tức mới</h3>
                                <div class="box-content">
                                    <ul class="cat-menu">

                                    </ul>
                                </div>
                            </aside>


                        </div>

                    </div>
                </div>
            </div>
        <?php }
    }
}



