<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/8/2018
 * Time: 9:21 AM
 */
if (!class_exists('TNCP_qv_gsvh_footer')){
    class TNCP_qv_gsvh_footer extends TNCP_ToanNang{
        protected $options = [
                'gs_footer' => ''
        ];
        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }
        /*Add html to Render*/
        public function render(){
            $info = $this->getOption('gs_footer');
            ?>
            <footer class="qv_gsvh_footer">
               <div class="width">
                  <div class="group">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="title">
                            <span>THÔNG TIN LIÊN HỆ</span>
                            <div></div>
                          </div>
                          <p><span><img src="<?php echo $this->getPath()?>images/icon-f_03.png"></span><span><?php echo  $info['adress']; ?></span></p>
                          <p><span><img src="<?php echo $this->getPath()?>images/icon-f_07.png"></span><span>Điện Thoại: <strong><?php echo  $info['hotline']; ?></strong></span></p>
                          <p><span><img src="<?php echo $this->getPath()?>images/icon-f_10.png"></span><span>Email: <?php echo  $info['email']; ?></span></p>
                          <p><span><img src="<?php echo $this->getPath()?>images/icon-f_14.png"></span><span>Website: <?php echo  $info['website']; ?></span></p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="title">
                            <span>dành cho phụ huynh</span>
                            <div></div>
                          </div>
                            <?php
                            wp_nav_menu( array(
                                    'theme_location' => 'phuhuynh-menu',
                                    'container' => 'false',
                                    'menu_id' => '',
                                    'menu_class' => '',
                                )
                            ); ?>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="title">
                            <span>dành cho gia sư</span>
                            <div></div>
                          </div>
                            <?php
                            wp_nav_menu( array(
                                    'theme_location' => 'giasunh-menu',
                                    'container' => 'false',
                                    'menu_id' => '',
                                    'menu_class' => '',
                                )
                            ); ?>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="title">
                            <span>Đăng ký nhận tin</span>
                            <div></div>
                          </div>
                          <div class="jest">Đừng bỏ lỡ hàng ngàn sản phẩm và chương trình siêu hấp dẫn</div>
                          <div class="search">
                            <?php echo do_shortcode( '[gravityform id=3 title=false description=false ajax=true]');?>
                          </div>
                          <div class="mxh">
                            <span>KẾT NỐI VỚI CHÚNG TÔI</span>
                            <a href="<?php echo  $info['fanpage']; ?>" target="_blank"><img src="<?php echo $this->getPath()?>images/mxh_03.png"></a>
                            <a href="<?php echo  $info['youtube']; ?>" target="_blank"><img src="<?php echo $this->getPath()?>images/mxh_05.png"></a>
                            <a href="<?php echo  $info['skype']; ?>" target="_blank"><img src="<?php echo $this->getPath()?>images/mxh_07.png"></a>
                            <a href="<?php echo  $info['google']; ?>" target="_blank"><img src="<?php echo $this->getPath()?>images/mxh_09.png"></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
               </div>
               <div class="copyright">
                  Copyright © 2018 - All right Reserved <a href="#">GiaSuVuiHoc</a>
                </div>
            </footer>
        <?php }
    }
}