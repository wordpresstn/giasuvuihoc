<?php

if (!class_exists('TNCP_qv_gsvh_service')) {
    class TNCP_qv_gsvh_service extends TNCP_ToanNang
    {
        protected $options = [
                'gs_home_abouts' => ''
        ];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render() {
            $abouts = $this->getOption('gs_home_abouts');
            ?>

            <div class="qv_gsvh_service">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <?php if($abouts):
                                foreach ($abouts as $item){
                            ?>
                            <div class="col-md-3 col-sm-6 col-xs-12 box">
                                <div class="item">
                                    <a class="desc" href="#" title="">
                                        <span class="img">
                                            <img src="<?php echo $item['img']['url']; ?>" alt="">
                                        </span>
                                        <h3><?php echo $item['title']; ?></h3>
                                        <p><?php echo $item['content']; ?></p>
                                    </a>
                                </div>
                            </div>
                            <?php } endif; ?>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
