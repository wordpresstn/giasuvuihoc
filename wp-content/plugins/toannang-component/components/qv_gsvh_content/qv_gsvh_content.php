<?php

if (!class_exists('TNCP_qv_gsvh_content')) {
    class TNCP_qv_gsvh_content extends TNCP_ToanNang
    {
        protected $options = [
            'gs_home_gioithieu' => '',
            'gs_home_banner' => '',
        ];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render() {
            $content = $this->getOption('gs_home_gioithieu');
            $banner = $this->getOption('gs_home_banner');
            ?>

            <div class="qv_gsvh_content">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo do_shortcode('[echo_sidebar_giasuvuihoc]'); ?>
                            </div>

                            <div class="col-md-9">
                                <!-- TRANG NỘI DUNG TRANG CHỦ-->
                                <div class="qv_content_noidung">
                                    <?php echo $content['content'];?>
                                </div>                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
