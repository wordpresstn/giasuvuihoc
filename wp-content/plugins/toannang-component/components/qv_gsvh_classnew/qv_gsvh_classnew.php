<?php

if (!class_exists('TNCP_qv_gsvh_classnew')) {
    class TNCP_qv_gsvh_classnew extends TNCP_ToanNang
    {
        protected $options = [];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()   {
            global $post;
            ?>
            <div class="qv_gsvh_classnew">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="title">
                                    <span>các lớp cần gia sư</span>
                                    <p>Đăng ký các lớp dạy kèm phù hợp với bạn</p>
                                    <div></div>
                                </div>
                                <form action="<?php bloginfo('url'); ?>/" method="GET" role="form" class="timkiem">
                                    <div class="list">
                                        <input type="hidden" name="post_type" value="class">

                                        <div class="paxt">
                                            <label>Chọn khu vực :</label>
                                            <select name="product_cat" id="inputProduct_cat" class="form-control" >

                                                <span class="caret"></span>
                                                <?php
                                                $parent_cat_arg = array('hide_empty' => false,'orderby'  => 'menu_order', );
                                                $parent_cat = get_terms('tax_location',$parent_cat_arg); ?>
                                                <?php foreach ($parent_cat as  $catVal) {  ?>
                                                    <option value="<?php echo $catVal->slug; ?>"><?php echo $catVal->name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="paxt">
                                            <label>Chọn Lớp :</label>
                                            <select name="hoan-thien" id="inputHoan-thien" class="form-control" >
                                                <span class="caret"></span>
                                                <?php
                                                $parent_cat_arg = array('hide_empty' => false, 'parent' => 0, 'orderby'  => 'menu_order', );
                                                $parent_cat = get_terms('tax_level',$parent_cat_arg); ?>

                                                <?php foreach ( $parent_cat as $cate ) { ?>
                                                    <option value="<?php echo $cate->slug; ?>"><?php echo $cate->name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>


                                            <button type="submit" class="btn btn-primary"><?php _e('Tìm kiếm','tn_component');?></button>

                                    </div
                                </form>

                                <div class="group">
                                    <?php while(have_posts()): the_post();?>
                                    <?php
                                        $tax_sub = get_the_terms( get_the_ID(), 'tax_subjects' );
                                        $tax_level = get_the_terms( get_the_ID(), 'tax_level' );
                                        $tax_time = get_the_terms( get_the_ID(), 'tax_time' );
                                        $tax_location = get_the_terms( get_the_ID(), 'tax_localtion' );
                                        $tax_monhoc = get_the_terms( get_the_ID(), 'tax_monhoc' );

                                    ?>
                                    <div class="item">
                                        <h3>Mã lớp <?php the_title(); ?></h3>
                                        <div class="desc">
                                            <p><strong>Người học : </strong><?php  the_field('class_people') ?></p>
                                            <p><strong>Lớp dạy : </strong><?php 
                                                if($tax_level){foreach ($tax_level as $item ){echo $item->name; }} 
                                            ?></p>
                                            <p><strong>Môn dạy : </strong><?php foreach ($tax_monhoc as $item ){echo $item->name; } ?></p>
                                            <p><strong>Địa chỉ : </strong><?php the_field('class_adress')?><a href="<?php the_field('class_adress_link'); ?>" target="_blank">(Xem bản đồ)</a></p>
                                            <p><strong>Mức lương : </strong><strong class="price"><?php  the_field('class_salary') ?> đồng/tháng</strong></p>
                                            <p><strong>Thời gian : </strong><span class="time">
                                                <?php if($tax_time) {foreach ($tax_time as $item ){echo $item->name; }} ?>
                                                </span></p>
                                            <p><strong>Số buổi : </strong><?php  the_field('class_sessions') ?></p>
                                            <p><strong>Yêu cầu : </strong><?php  the_field('class_request') ?></p>
                                            <p><strong>Liên hệ : </strong><?php  the_field('class_contact') ?></p>
                                            <p class="voice"><?php  the_field('class_request_more') ?></p>
                                            <a href="#" class="btn-book"><i class="fas fa-sign-out-alt"></i>đăng ký nhận lớp</a>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                </div>

                                <div class="text-center">
                                    <a href="#" class="view-more">Xem tất cả</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
