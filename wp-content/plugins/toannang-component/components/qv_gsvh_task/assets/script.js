
$('.qv_gsvh_task__group').slick({
  arrows: false,
  dots: false,
  slidesToShow: 6,
  slidesToScroll: 1,
  infinite: true,
  speed: 700,
  autoplay: true,
  autoplaySpeed: 4000,
  prevArrow:'<div class="btn-slider-left"><i class="fas fa-chevron-left"></i></div>',
  nextArrow:'<div class="btn-slider-right"><i class="fas fa-chevron-right"></i></div>',
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      }
    }
  ]

});