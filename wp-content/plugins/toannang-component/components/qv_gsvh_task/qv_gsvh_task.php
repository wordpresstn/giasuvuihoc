<?php

if (!class_exists('TNCP_qv_gsvh_task')) {
    class TNCP_qv_gsvh_task extends TNCP_ToanNang
    {
        protected $options = [
                'gs_home_paner' => ''
        ];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render() {
            $doitac = $this->getOption('gs_home_paner');
            ?>
                <div class="qv_gsvh_task">
                    <div class="width">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12">       
                                    <div class="qv_gsvh_task__group">
                                        <?php if($doitac):
                                            foreach ($doitac as $item){
                                        ?>
                                        <div class="qv_gsvh_task__items">
                                            <a href="<?php echo $item['link']; ?>">
                                                <img src="<?php echo $item['img']['url']; ?>">
                                            </a>
                                        </div>
                                        <?php } endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
        }
    }
}