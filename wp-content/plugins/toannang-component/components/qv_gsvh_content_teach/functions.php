<?php
if (!function_exists('render_col_left')):
    function render_col_left()
    {
        ?>
        <div class="col-md-3">
            <div class="qv_content_list">
                <div class="list">
                    <h3>Dành cho phụ huynh</h3>
                    <ul>
                        <li><a href="#">- Tư vấn chọn gia sư</a></li>
                        <li><a href="#">- Đăng ký tìm gia sư</a></li>
                        <li><a href="#">- Học phí tham khảo</a></li>
                        <li><a href="#">- Dịch vụ gia sư</a></li>
                    </ul>
                </div>
                <div class="list">
                    <h3>Dành cho gia sư vui học</h3>
                    <ul>
                        <li><a href="#">- Đăng ký làm gia sư</a></li>
                        <li><a href="#">- Đăng ký làm gia sư</a></li>
                        <li><a href="#">- Lớp dạy hiện có</a></li>
                        <li><a href="#">- Tuyển dụng</a></li>
                    </ul>
                </div>
                <div class="group">
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_03.jpg" alt="">
                            <span>Gia sư theo quận</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_06.jpg" alt="">
                            <span>Gia sư theo lớp</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_08.jpg" alt="">
                            <span>Gia sư luyện thi</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_11.jpg" alt="">
                            <span>Gia sư theo môn</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_13.jpg" alt="">
                            <span>Gia sư ngoại ngữ</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_16.jpg" alt="">
                            <span>Gia sư các môn năng khiếu</span>
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="/assets/images/bn_19.jpg" alt="">
                            <span>Gia sư dạy trực tuyến</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
endif;

if (!function_exists('lept_register_tutor')):
    add_action('register_tutor', 'lept_register_tutor', 10, 1);
    function lept_register_tutor($data = array())
    {
        $form_id = 455;
        $post_id = 411;
        $form_settings = array();

        $tax_subjects = get_terms(array(
            'taxonomy' => 'tax_subjects',
            'hide_empty' => false,
        ));
        $tax_monhoc = get_terms(array(
            'taxonomy' => 'tax_monhoc',
            'hide_empty' => false,
        ));
        $tax_location = get_terms(array(
            'taxonomy' => 'tax_location',
            'hide_empty' => false,
        ));
        $tax_time = get_terms(array(
            'taxonomy' => 'tax_time',
            'hide_empty' => false,
        ));
        $tax_truong = get_terms(array(
            'taxonomy' => 'tax_truong',
            'hide_empty' => false,
        ));
        $tax_level = get_terms(array(
            'taxonomy' => 'tax_level',
            'hide_empty' => false,
        ));

        ?>

        <form id="register_tutor" class="form">
            <!--<input type="hidden" name="url" value="<?/*= home_url()*/
            ?>/wp-admin/admin-ajax.php" >
            <input type="hidden" name="action" value="wpuf_submit_register" >
            <input type="hidden" name="form_id" value="475" >
            <input type="hidden" name="page_id" value="411" >
            <input type="hidden" name="delete_attachments[]" value="" >
            <input type="hidden" name="_wpnonce" value="d79bb5e7f4" >
            <input type="hidden" name="_wp_http_referer" value="/dang-ky-day/" >-->
            <div class="item">
                <span><i class="fas fa-user"></i> Họ tên(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Vui lòng nhập họ và tên" name="display_name">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-male"></i><i class="fas fa-female"></i> Giới tính(*)</span>
                <div class="box">
                    <label class="radio-inline"><input type="radio" name="gender"
                                                       checked>Nam</label>
                    <label class="radio-inline"><input type="radio"
                                                       name="gender">Nữ</label>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-location-arrow"></i> Địa chỉ(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Địa chỉ"
                           name="address">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-birthday-cake"></i> Năm sinh(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Năm sinh"
                           name="yearborn">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-location-arrow"></i> Nơi sinh(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Nơi sinh"
                           name="whereborn">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-address-card"></i> Số CMND(*)</span>
                <div class="box">
                    <input type="text" class="form-control"
                           placeholder="Vui lòng nhập số CMND của bạn" name="idpersonal">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-phone"></i> Điện thoại(*)</span>
                <div class="box">
                    <input type="text" class="form-control"
                           placeholder="Vui lòng nhập số điện thoại" name="telephone">
                </div>
            </div>
            <div class="item hidden">
                <span><i class="fas fa-envelope"></i> Email(*)</span>
                <div class="box">
                    <input type="text" class="form-control"
                           placeholder="Vui lòng nhập email" name="email">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-image"></i> Hình đại diện(*)</span>
                <div class="box">
                    <div class="img_user">

                    </div>
                    <input type="file" placeholder="Choose" name="avatar"
                           style="display: inline-block;position: absolute;top: 35%;">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-image"></i> Bằng cấp</span>
                <div class="box">
                    <input type="file" multiple="multiple" placeholder="Choose" name="imglevel"
                           style="padding: 5px 10px;">
                    <strong class="text">Bằng cấp hoặc thẻ sinh viên (có thể chọn nhiều
                        hình)</strong>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-level-up-alt"></i> Trình độ(*)</span>
                <div class="box">
                    <select class="form-control" name="tax_level">
                        <option>Vui lòng chọn</option>
                        <?php foreach ($tax_level as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>"><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-graduation-cap"></i> Tên trường(*)</span>
                <div class="box">
                    <select class="form-control" name="tax_time">
                        <option>Vui lòng chọn</option>
                        <?php foreach ($tax_truong as $key => $value) { ?>
                            <option value="<?= $value->term_id ?>"><?= $value->name ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-graduation-cap"></i> Chuyên nghành(*)</span>
                <div class="box">
                    <input type="text" class="form-control"
                           placeholder="Vui lòng nhập chuyên nghành" name="">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-glass-martini"></i> Dạy các lớp(*)</span>
                <div class="box">
                    <?php foreach ($tax_subjects as $key => $value) { ?>
                        <label class="checkbox-inline">
                            <input name="tax_subjects[]" type="checkbox"
                                   value="<?= $value->term_id ?>"><?= $value->name ?></label>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-bars"></i> Dạy các môn(*)</span>
                <div class="box">
                    <?php foreach ($tax_monhoc as $key => $value) { ?>
                        <label class="checkbox-inline"><input name="tax_monhoc[]" type="checkbox"
                                                              value="<?= $value->term_id ?>"><?= $value->name ?></label>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-map-marker-alt"></i> Khu vực dạy(*)</span>
                <div class="box">
                    <?php foreach ($tax_location as $key => $value) { ?>
                        <label class="checkbox-inline"><input name="tax_location[]" type="checkbox"
                                                              value="<?= $value->term_id ?>"><?= $value->name ?></label>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-calendar-alt"></i> Thời gian dạy(*)</span>
                <div class="box">
                    <?php foreach ($tax_time as $key => $value) { ?>
                        <label class="checkbox-inline"><input name="tax_monhoc[]" type="checkbox"
                                                              value="<?= $value->term_id ?>"><?= $value->name ?></label>
                    <?php } ?>
                </div>
            </div>
            <div class="item">
                <span><i class="fa fa-money"></i> Lương yêu cầu(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Mức lương mong muốn" name="salary">
                </div>
            </div>
            <div class="item">
                <span><i class="fas fa-sticky-note"></i> Mô tả thêm</span>
                <div class="box">
                    <textarea class="form-control" rows="2" name="comment"
                              placeholder="Vui lòng nhập nội dung"></textarea>
                </div>
            </div>
            <div class="item">
                <span><i class="fa fa-key"></i> Captcha(*)</span>
                <div class="box">
                    <input type="text" class="form-control" placeholder="Mã captcha" name="code">
                </div>
                <div class="pache_button">
                    <?php
                    $code = '';
                    if (isset($_SESSION['captcha']) && isset($_REQUEST['code']) && $_SESSION['captcha'] == strtolower($_REQUEST['code'])) {

                    } else {
                        ?>
                        <img src="/assets/captcha.php" class="img-capcha" id="imgcaptcha" alt="captcha">
                        <input type="hidden" name="act" value="<?= $code ?>">
                        <?php
                    }
                    ?>
                    <a><img onclick="reloadCaptcha();" src="/assets/images/reload.png"></a>
                </div>
            </div>
            <div class="text-center hidden" id="rgtutor">
                <button>Đăng ký</button>
            </div>
        </form>
        <?php
    }
endif;

if (!function_exists('lept_tax_level')):
    add_action('register_tax_level', 'lept_tax_level', 10, 1);
    function lept_tax_level($data = array())
    {
        $tax_level = get_terms(array(
            'taxonomy' => 'tax_level',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-level-up-alt"></i> Trình độ</span>
        <div class="box">
            <select class="form-control" name="tax_level">
                <option>Vui lòng chọn</option>
                <?php foreach ($tax_level as $key => $value) { ?>
                    <option value="<?= $value->term_id ?>"><?= $value->name ?></option>
                <?php } ?>
            </select>
        </div>
        <?php
    }
endif;
if (!function_exists('lept_tax_truong')):
    add_action('register_tax_truong', 'lept_tax_truong', 10, 1);
    function lept_tax_truong($data = array())
    {
        $tax_truong = get_terms(array(
            'taxonomy' => 'tax_truong',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-graduation-cap"></i> Tên trường</span>
        <div class="box">
            <select class="form-control" name="tax_time">
                <option>Vui lòng chọn</option>
                <?php foreach ($tax_truong as $key => $value) { ?>
                    <option value="<?= $value->term_id ?>"><?= $value->name ?></option>
                <?php } ?>
            </select>
        </div>
        <?php
    }
endif;
if (!function_exists('lept_tax_time')):
    add_action('register_tax_time', 'lept_tax_time', 10, 1);
    function lept_tax_time($data = array())
    {
        $tax_time = get_terms(array(
            'taxonomy' => 'tax_time',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-calendar-alt"></i> Thời gian dạy</span>
        <div class="box">
            <?php foreach ($tax_time as $key => $value) { ?>
                <label class="checkbox-inline"><input name="tax_monhoc[]" type="checkbox"
                                                      value="<?= $value->term_id ?>"><?= $value->name ?></label>
            <?php } ?>
        </div>
        <?php
    }
endif;
if (!function_exists('lept_tax_location')):
    add_action('register_tax_location', 'lept_tax_location', 10, 1);
    function lept_tax_location($data = array())
    {
        $tax_location = get_terms(array(
            'taxonomy' => 'tax_location',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-map-marker-alt"></i> Khu vực dạy</span>
        <div class="box">
            <?php foreach ($tax_location as $key => $value) { ?>
                <label class="checkbox-inline"><input name="tax_location[]" type="checkbox"
                                                      value="<?= $value->term_id ?>"><?= $value->name ?></label>
            <?php } ?>
        </div>
        <?php
    }
endif;
if (!function_exists('lept_tax_monhoc')):
    add_action('register_tax_monhoc', 'lept_tax_monhoc', 10, 1);
    function lept_tax_monhoc($data = array())
    {
        $tax_monhoc = get_terms(array(
            'taxonomy' => 'tax_monhoc',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-bars"></i> Dạy các môn</span>
        <div class="box">
            <?php foreach ($tax_monhoc as $key => $value) { ?>
                <label class="checkbox-inline"><input name="tax_monhoc[]" type="checkbox"
                                                      value="<?= $value->term_id ?>"><?= $value->name ?></label>
            <?php } ?>
        </div>
        <?php
    }
endif;
if (!function_exists('lept_tax_subjects')):
    add_action('register_tax_subjects', 'lept_tax_subjects', 10, 1);
    function lept_tax_subjects($data = array())
    {
        $tax_subjects = get_terms(array(
            'taxonomy' => 'tax_subjects',
            'hide_empty' => false,
        ));
        ?>
        <span><i class="fas fa-glass-martini"></i> Dạy các lớp</span>
        <div class="box">
            <?php foreach ($tax_subjects as $key => $value) { ?>
                <label class="checkbox-inline">
                    <input name="tax_subjects[]" type="checkbox" value="<?= $value->term_id ?>"><?= $value->name ?>
                </label>
            <?php } ?>
        </div>
        <?php
    }
endif;
?>

