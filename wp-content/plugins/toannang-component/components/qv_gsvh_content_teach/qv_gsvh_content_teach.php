<?php
if (!class_exists('TNCP_qv_gsvh_content_teach')) {
    class TNCP_qv_gsvh_content_teach extends TNCP_ToanNang
    {
        protected $options = [];
        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()
        {
            ?>
            <div class="qv_gsvh_content_teach" >
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo do_shortcode('[echo_sidebar_giasuvuihoc]'); ?>
                            </div>
                            <div class="col-md-9">
                                <!-- TRANG ĐĂNG KÝ DẠY-->
                                <div class="qv_form_register">
                                    <span class="title"> Hồ sơ gia sư </span>
                                    <div class="content">
                                        <h4>đăng ký làm gia sư</h4>
                                        <?php //do_action('register_tutor', array()); ?>
                                        <?php  echo do_shortcode('[wpuf_profile type="registration" id="522"] [wpuf_profile type="profile" id="522"]'); ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
}
