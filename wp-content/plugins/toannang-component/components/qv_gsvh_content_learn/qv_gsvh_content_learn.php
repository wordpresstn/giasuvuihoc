<?php

if (!class_exists('TNCP_qv_gsvh_content_learn')) {
    class TNCP_qv_gsvh_content_learn extends TNCP_ToanNang
    {
        protected $options = [];

        function __construct()
        {
            parent::__construct(__FILE__);
            parent::setOptions($this->options);
        }

        public function render()
        {
            ?>

            <div class="qv_gsvh_content_learn">
                <div class="width">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="qv_content_list">

                                </div>
                            </div>

                            <div class="col-md-9">
                                <!-- TRANG ĐĂNG KÝ HỌC -->
                                <div class="qv_form_register">
                                    <span class="title">
                                        Đăng ký học
                                    </span>
                                    <div class="content">
                                        <h4>đăng ký tìm gia sư</h4>

                                        <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>

                                    </div>
                                </div>                      
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
        }
    }
}
