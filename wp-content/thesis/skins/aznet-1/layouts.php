<?php

global $post, $page_info, $page_location;

$page_info = get_field('page_info', 'option');
$page_location = get_field('main_location', 'option');

/**
 * HOOK: HEADER
 */
add_action('tnc_theme_header', function () use ($page_info) {

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_header',array(
        'gs_header_info' => get_field( 'gs_header_info', 'option'),
    ));

});


/**
 * HOOK: HOME PAGE -----------------------------------------------------------------------------------------------------
 */

add_action('tnc_theme_home', function () {

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_banner',array());
    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_service',array(
        'gs_home_abouts' => get_field( 'gs_home_about', 'option'),
    ));
    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_content',array(
        'gs_home_gioithieu' => get_field( 'gs_home_gioithieu', 'option'),
        'gs_home_banner' => get_field( 'gs_home_banner_doc', 'option'),
    ));
    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_task',array(
        'gs_home_paner'  => get_field( 'gs_home_paner', 'option'),
    ));


});

/**
 * HOOK: PAGE ----------------------------------------------------------------------------------------------------------
 */

add_action('tnc_theme_page', function () use ($post, $page_location) {
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_page',$args);
});

/**
 * HOOK: ARCHIVE -------------------------------------------------------------------------------------------------------
 */

add_action('tnc_theme_archive', function () {
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('tncp_th_nguyenhung_arhive',$args);
});

add_action('tnc_theme_archive_product', function () {
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $danhmuc = get_terms( 'product_cat', array() );
    $args = ['products-cat' => $danhmuc];
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_archive_product',$args);
});

/**
 * HOOK: SINGLE --------------------------------------------------------------------------------------------------------
 */

add_action('tnc_theme_single', function () use ($post, $page_info) {

    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('tncp_th_anpha_single_video',$args);

});

add_action('tnc_theme_single_product', function(){

    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('tncp_hc_khangphuc_single_product',$args);

});

/**
 * HOOK: SEARCH --------------------------------------------------------------------------------------------------------
 */
add_action('tnc_theme_search', function(){

    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('TNCP_hc_vape_archive_1',$args);

});

/**
 * HOOK: FULL PAGE -----------------------------------------------------------------------------------------------------
 */

add_action('tnc_theme_page_full', function () use ($post, $page_location) {
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_breadcrumb', array('khangphuc-breadcrumb-banner'=> get_field('banner_breadcrumbs','option')));
    $args = [];
    TNCP_ToanNang::renderComponent('TNCP_hc_khangphuc_page',$args);
});

 /**
  * Hook: Bảng giá
  */
aznet_append_hook('ContainerBangGia',function (){

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_table',array(

    ));

},'before');

/**
 * Hook: GIA SƯ
 */
aznet_append_hook('ContainerPageGiasu',function (){

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_content_tutor',array(

    ));

},'before');

/**
 * Hook: LỚP MỚI
 */
aznet_append_hook('ContainerPageLopmoi',function (){

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_classnew',array(

    ));

},'before');

/**
 * Hook: ĐĂNG KÝ DẠY
 */
aznet_append_hook('ContainerDangKyDay',function (){

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_content_teach',array(

    ));

},'before');

/**
 * Hook: ĐĂNG KÝ HỌC
 */
aznet_append_hook('ContainerDangKyHoc',function (){

    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_content_learn',array(

    ));

},'before');

/**
 * HOOK: FOOTER
 */
add_action('tnc_theme_footer', function () {


    TNCP_ToanNang::renderComponent('TNCP_qv_gsvh_footer',array(
        'gs_footer' => get_field( 'gs_footer', 'option'),
    ));

});

/**
 * HOOK: PAGE 404
 */
add_action('container_page_404', function () {

    $args = array();
    TNCP_ToanNang::renderComponent('tncp_hc_404_page',$args);

});